# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Package building the dSFMT libraries as part of the ATLAS offline software
# build.
#

# The name of the package.
atlas_subdir( dSFMT )

# The source code for dSFMT.
set( ATLAS_DSFMT_SOURCE "${CMAKE_CURRENT_SOURCE_DIR}/src/dSFMT-src-2.1.tar.gz"
   CACHE STRING "dSFMT source file to use" )
set( ATLAS_DSFMT_HASH "b3a38dac7fd8996a70d02edc4432dd75"
   CACHE STRING "MD5 hash for the dSFMT source file" )
mark_as_advanced( ATLAS_DSFMT_SOURCE ATLAS_DSFMT_HASH )

# Directory to put the intermediate build results in.
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/dSFMTBuild" )

# Generate the Makefile used for the build.
set( SSE2FLAGS )
if( "${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "x86_64" )
   set( SSE2FLAGS "-msse2 -DHAVE_SSE2" )
endif()
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/src/Makefile.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/Makefile" @ONLY )

# Build the package for the build area.
ExternalProject_Add( dSFMT
   PREFIX "${CMAKE_BINARY_DIR}"
   URL "${ATLAS_DSFMT_SOURCE}"
   URL_MD5 "${ATLAS_DSFMT_HASH}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   PATCH_COMMAND ${CMAKE_COMMAND} -E copy
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/Makefile"
   "<SOURCE_DIR>/Makefile"
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E echo
   "Configuring the build of dSFMT"
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   BUILD_IN_SOURCE 1 )
add_dependencies( Package_dSFMT dSFMT )

# Install it.
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module.
install( FILES "cmake/FinddSFMT.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )
