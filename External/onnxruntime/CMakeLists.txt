# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building ONNX Runtime as part of the offline / analysis
# release.
#

# This package needs CMake 3.18 at least.
cmake_minimum_required( VERSION 3.18 )

# Set the name of the package:
atlas_subdir( onnxruntime )

# The source code of onnxruntime:
set( ATLAS_ONNXRUNTIME_SOURCE
   "http://cern.ch/atlas-software-dist-eos/externals/onnxruntime/onnxruntime-v1.5.1.tar.bz2"
   CACHE STRING "OnnxRuntime source file to use" )
set( ATLAS_ONNXRUNTIME_HASH "bd17c0d085c13bab112e7d92c68b091f"
   CACHE STRING "MD5 hash for the OnnxRuntime source file" )
mark_as_advanced( ATLAS_ONNXRUNTIME_SOURCE ATLAS_ONNXRUNTIME_HASH )

# Temporary directory for the build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/onnxruntimeBuild" )

# Extra arguments for the build configuration:
set( _extraArgs )
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Release" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=Release )
elseif( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( NOT "${CMAKE_CXX_STANDARD}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# Find all necessary externals.
if( ATLAS_BUILD_PYTHON )
   set( Python_EXECUTABLE "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/python3" )
else()
   find_package( Python COMPONENTS Interpreter )
endif()
find_package( ZLIB )
find_package( PNG )

# Find the optional externals.
option( ATLAS_ONNXRUNTIME_USE_CUDA
   "Use the CUDA capabilities of ONNX Runtime, if possible" TRUE )
mark_as_advanced( ATLAS_ONNXRUNTIME_USE_CUDA )
if( ATLAS_ONNXRUNTIME_USE_CUDA )
   find_package( CUDAToolkit )
   find_package( cuDNN )
   if( CUDAToolkit_FOUND AND CUDNN_FOUND )
      message( STATUS "Building onnxruntime with CUDA support" )
      list( APPEND _extraArgs
         -DCMAKE_PREFIX_PATH:PATH=${CUDAToolkit_LIBRARY_ROOT}
         -Donnxruntime_USE_CUDA:BOOL=TRUE
         -Donnxruntime_CUDA_HOME:STRING=${CUDAToolkit_LIBRARY_ROOT}
         -Donnxruntime_CUDNN_HOME:STRING=$<TARGET_PROPERTY:cuDNN::cudnn,INSTALL_PATH> )
   endif()
endif()

# Build onnxruntime.
ExternalProject_Add( onnxruntime
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   URL "${ATLAS_ONNXRUNTIME_SOURCE}"
   URL_MD5 "${ATLAS_ONNXRUNTIME_HASH}"
   SOURCE_SUBDIR "cmake"
   PATCH_COMMAND patch -p1 <
   "${CMAKE_CURRENT_SOURCE_DIR}/patches/onnxruntime-1.5.1-cmake.patch"
   CMAKE_CACHE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
   -Donnxruntime_BUILD_SHARED_LIB:BOOL=ON
   -Donnxruntime_BUILD_UNIT_TESTS:BOOL=OFF
   -DPYTHON_EXECUTABLE:FILEPATH=${Python_EXECUTABLE}
   -DZLIB_INCLUDE_DIR:PATH=${ZLIB_INCLUDE_DIR}
   -DZLIB_LIBRARY:FILEPATH=${ZLIB_LIBRARY}
   -DPNG_PNG_INCLUDE_DIR:PATH=${PNG_PNG_INCLUDE_DIR}
   -DPNG_LIBRARY:FILEPATH=${PNG_LIBRARY}
   ${_extraArgs}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( onnxruntime forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo
   "Forcing the re-download of onnxruntime (2020.04.20.)"
   DEPENDERS download )
ExternalProject_Add_Step( onnxruntime purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for onnxruntime."
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( onnxruntime buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing onnxruntime into the build area"
   DEPENDEES install )
add_dependencies( Package_onnxruntime onnxruntime )
if( ATLAS_BUILD_PYTHON )
   add_dependencies( onnxruntime Python )
endif()

# Install onnxruntime.
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
install( FILES "cmake/Findonnxruntime.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )
