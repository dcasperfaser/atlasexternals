# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Package building FastJet as part of the offline software build.
#

# Set the package name:
atlas_subdir( FastJet )

# The source code of FastJet:
set( ATLAS_FASTJET_SOURCE
   "http://cern.ch/lcgpackages/tarFiles/sources/fastjet-3.4.0.tar.gz"
   CACHE STRING "FastJet source file to use" )
set( ATLAS_FASTJET_HASH "69879b19006fb6dc7d0b98d01c5cd115"
   CACHE STRING "MD5 hash for the FastJet source file" )
mark_as_advanced( ATLAS_FASTJET_SOURCE ATLAS_FASTJET_HASH )

# Decide whether to request debug symbols from the build:
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
   set( _fastJetExtraConfig "--enable-debug" )
else()
   set( _fastJetExtraConfig "--disable-debug" )
endif()

# Temporary directory for the build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/FastJetBuild" )

# Extra environment options for the configuration.
set( _cflags )
set( _ldflags )
if( CMAKE_OSX_SYSROOT )
   list( APPEND _cflags -isysroot "${CMAKE_OSX_SYSROOT}"
                        -I "${CMAKE_OSX_SYSROOT}/usr/include" )
   list( APPEND _ldflags -isysroot "${CMAKE_OSX_SYSROOT}" )
endif()

# Specify optimisation flags explicitly, as 3.4.0 doesn't seem to do that itself
# anymore. But only do this for "known" compilers, not to intentionally break
# the build with more exotic ones.
if( ( "${CMAKE_CXX_COMPILER_ID}" MATCHES "GNU" ) OR
    ( "${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang" ) )
   if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
      list( APPEND _cflags -O0 )
   else()
      list( APPEND _cflags -O2 )
   endif()
endif()

# Massage the options to make them usable in the configuration script.
string( REPLACE ";" " " _cflags "${_cflags}" )
string( REPLACE ";" " " _ldflags "${_ldflags}" )

# Create the helper scripts.
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/sanitizeFastJet.sh.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeFastJet.sh"
   @ONLY )
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/configure.sh.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh"
   @ONLY )

# Set up the build of FastJet for the build area:
ExternalProject_Add( FastJet
   PREFIX "${CMAKE_BINARY_DIR}"
   URL "${ATLAS_FASTJET_SOURCE}"
   URL_MD5 "${ATLAS_FASTJET_HASH}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   CONFIGURE_COMMAND
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh"
   INSTALL_COMMAND make install
   COMMAND
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeFastJet.sh"
   COMMAND ${CMAKE_COMMAND} -E copy_directory
   "${_buildDir}/" "<INSTALL_DIR>" )
ExternalProject_Add_Step( FastJet forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo
   "Forcing the re-download of FastJet (2021.06.30.)"
   DEPENDERS download )
ExternalProject_Add_Step( FastJet purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for FastJet"
   DEPENDEES download
   DEPENDERS patch )
add_dependencies( Package_FastJet FastJet )

# Set up its installation:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
